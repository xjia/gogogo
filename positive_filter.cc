#include "positive_filter.h"
#include "gogogo.h" // constants
#include <sstream>
#include <string>
#include <vector>
#include "logger.h"
using namespace std;

PositiveFilter::PositiveFilter(int n, const string &power_levels, double l, double h): n(n), power_level(n, vector<double>(n, 0)), low(l), high(h) {
  istringstream iss(power_levels);
  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      iss >> power_level[i][j];
}

string PositiveFilter::getFilterName() {
  return "PositiveFilter";
}

pair<double, double> PositiveFilter::getThresholds() {
  return make_pair(low, high);
}

vector<vector<double> > PositiveFilter::generatePowerMap(int board_size, int board[], int color) {
  vector<vector<double> > power_map(board_size, vector<double>(board_size, 0));
  for (int i = 0; i < board_size; i++)
    for (int j = 0; j < board_size; j++) {
      int sign = 0;
      int c = board[i * board_size + j];
      if (c == color)
        sign = 1;
      else if (c == OTHER_COLOR(color))
        sign = 0;
      else
        continue;
      spread_out_power(i, j, sign, power_map);
    }
  Logger::getInstance().debug("PowerFilter power_map");
  for (int i = 0; i < board_size; i++) {
    string s = "";
    for (int j = 0; j < board_size; j++)
      s += "\t" + to_s(power_map[i][j]);
    Logger::getInstance().debug(s);
  }
  return power_map;
}

void PositiveFilter::spread_out_power(int i, int j, int sign, vector<vector<double> > &board_power)
{
  const int d = n / 2;
  for (int a = -d; a <= d; a++)
    for (int b = -d; b <= d; b++) {
      int x = i + a;
      int y = j + b;
      if (on_board(x, y)) {
        board_power[x][y] += power_level[a + d][b + d] * sign;
      }
    }
}
