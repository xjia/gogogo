#ifndef FIXPOS_FILTER_H_
#define FIXPOS_FILTER_H_

#include <string>
#include <vector>
#include "filter.h"

class FixposFilter : public Filter {
public:
  FixposFilter(int n, const std::string &board_pattern): n(n), board_pattern(board_pattern) {}
  virtual ~FixposFilter() {}
  virtual std::string getFilterName();
  virtual std::pair<double, double> getThresholds();
  virtual std::vector<std::vector<double> > generatePowerMap(int board_size, int board[], int color);
private:
  int n;
  std::string board_pattern;
};

#endif
