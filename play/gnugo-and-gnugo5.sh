#!/bin/bash
GOGUI="/Applications/GoGui.app/Contents/bin/gogui"
TWOGTP="/Applications/GoGui.app/Contents/bin/gogui-twogtp"
BLACK="gnugo --mode gtp"
WHITE="gnugo --mode gtp --level 5"
$GOGUI -program "$TWOGTP -black \"$BLACK\" -white \"$WHITE\" -size 13"

