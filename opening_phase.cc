#include "opening_phase.h"
#include "fixpos_filter.h"

using std::string;

OpeningPhase::OpeningPhase() {
  addFilter(new FixposFilter(
    7,
    "x..x..x"
    "......."
    "......."
    "x..x..x"
    "......."
    "......."
    "x..x..x"
  ));
}

string OpeningPhase::getPhaseName() {
  return "OpeningPhase";
}
