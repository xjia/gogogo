#include "phase.h"
#include <iostream>
#include <vector>
#include <utility>
#include "logger.h"
#include "board.h"
#include "policy.h"
#include <cstdlib>
using namespace std;

bool wins_from_here(const pair<int, int> &move, int board_size, int board[], int color) {
  board_t bd(board_size, board);
  bd.put(color, move.first, move.second);
  while (true) {
    if (!bd.random_play(OTHER_COLOR(color))) break;
    if (!bd.random_play(color)) break;
  }
  return bd.score_of(color) > bd.score_of(OTHER_COLOR(color));
}

vector<double> calculate_scores_of(const vector<pair<int, int> > &moves, int board_size, int board[], int color) {
  vector<double> scores;
  for (size_t i = 0; i < moves.size(); i++) {
    const int try_max = 100 + rand() % 10;
    // const int try_max = 10;
    double win_count = 0;
    for (int try_count = 0; try_count < try_max; try_count++)
      if (wins_from_here(moves[i], board_size, board, color))
        win_count++;
    scores.push_back(win_count / try_max);
  }
  return scores;
}

bool Phase::selectMove(int board_size, int board[], int color, const vector<pair<int, int> > &moves, pair<int, int> &pt) {
  Logger::getInstance().trace(getPhaseName() + ": selectMove");
  for (size_t i = 0; i < filters.size(); i++) {
    Filter* f = filters[i];
    // cerr << "Phase::selectMove i=" << i << " f=" << f << endl;
    vector<pair<int, int> > filtered_moves = f->filter(board_size, board, color, moves);
    if (filtered_moves.empty()) continue;
    if (Policy::getInstance().getSearchFlag() && "RunningPhase" == getPhaseName()) {
      // evaluate cost function here
      vector<double> move_scores = calculate_scores_of(filtered_moves, board_size, board, color);
      double max_score = -1e9;
      for (size_t j = 0; j < move_scores.size(); j++)
        max_score = max(max_score, move_scores[j]);
      // filter out moves with max score
      vector<pair<int, int> > max_moves;
      for (size_t j = 0; j < move_scores.size(); j++)
        if (move_scores[j] == max_score)
          max_moves.push_back(filtered_moves[j]);
      // random on tie
      int random_index = rand() % max_moves.size();
      pt = max_moves[random_index];
    } else {
      // not that serious phase -- just random
      int index = rand() % filtered_moves.size();
      pt = filtered_moves[index];
    }
    Logger::getInstance().info(getPhaseName() + ": returning " + to_s(pt.first) + "," + to_s(pt.second));
    return true;
  }
  Logger::getInstance().info(getPhaseName() + ": returning false");
  return false;
}
