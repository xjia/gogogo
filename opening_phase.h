#ifndef OPENING_PHASE_H_
#define OPENING_PHASE_H_

#include "phase.h"

class OpeningPhase : public Phase {
public:
  OpeningPhase();
  virtual ~OpeningPhase() {}
  virtual std::string getPhaseName();
};

#endif
