#ifndef POLICY_H_
#define POLICY_H_

#include <vector>
#include <utility>
#include "phase.h"

class Policy {
public:
  static Policy& getInstance();
  static void destroyInstance();
protected:
  static Policy* _instance;

public:
  std::pair<int, int> selectMove(int board_size, int board[], int color, const std::vector<std::pair<int, int> > &moves);
  void setSearchFlag(bool f) { search_flag = f; }
  bool getSearchFlag() const { return search_flag; }
protected:
  Policy();
private:
  Phase *opening_phase, *running_phase, *closing_phase, *passing_phase;
  Phase* current_phase;
  bool search_flag;
};

#endif
