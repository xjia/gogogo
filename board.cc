#include "board.h"
#include "gogogo.h"
#include <vector>
#include <cstdlib>
using namespace std;

board_t::board_t(int in_board_size, int in_board[]): board_size(in_board_size), sub(board_size) {
  board = vector<vector<int> >(board_size, vector<int>(board_size, EMPTY));
  score = vector<int>(10, 0);
  sub.clear_board();
  for (int r = 0; r < board_size; r++)
    for (int c = 0; c < board_size; c++) {
      board[r][c] = in_board[r * board_size + c];
      if (board[r][c] != EMPTY)
        sub.play_move(r, c, board[r][c]);
    }
}

void board_t::put(int color, int row, int col) {
  if (board[row][col] == EMPTY) {
    board[row][col] = color;
    sub.play_move(row, col, color);
  } else {
    // TODO report fatal error
  }
}

int board_t::score_of(int color) const {
  int s = 0;
  for (int r = 0; r < board_size; r++)
    for (int c = 0; c < board_size; c++)
      if (board[r][c] == color)
        s++;
  return s + score[color];
}

/* Offsets for the four directly adjacent neighbors. Used for looping. */
static int deltai[4] = {-1, 1, 0, 0};
static int deltaj[4] = {0, 0, -1, 1};

bool board_t::random_play(int color) {
  vector<pair<int, int> > moves;
  for (int ai = 0; ai < board_size; ai++)
    for (int aj = 0; aj < board_size; aj++) {
      /* Consider moving at (ai, aj) if it is legal and not suicide. */
      if (board[ai][aj] == EMPTY && !sub.suicide(ai, aj, color)) {
        /* Further require the move not to be suicide for the opponent... */
        if (!sub.suicide(ai, aj, OTHER_COLOR(color))) {
          moves.push_back(make_pair(ai, aj));
        } else {
          /* ...however, if the move captures at least one stone,
           * consider it anyway.
           */
          for (int k = 0; k < 4; k++) {
            int bi = ai + deltai[k];
            int bj = aj + deltaj[k];
            if (on_board(bi, bj) && board[bi][bj] == OTHER_COLOR(color)) {
              moves.push_back(make_pair(ai, aj));
              break;
            }
          }
        }
      }
    }
  if (moves.size() <= 0) return false;
  int index = rand() % moves.size();
  pair<int, int> pt = moves[index];
  put(color, pt.first, pt.second);
  // TODO collect captured stones and increment score[color]
  return true;
}
