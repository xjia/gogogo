\documentclass[preprint,10pt]{sigplanconf}

% The following \documentclass options may be useful:
%
% 10pt          To set in 10-point type instead of 9-point.
% 11pt          To set in 11-point type instead of 9-point.
% authoryear    To obtain author/year citation style instead of numeric.

\usepackage{amsmath,amssymb,amsthm}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[pdf]{pstricks}
\usepackage{psgo}
\usepackage{multirow}

\newtheorem{example}{Example}
\newcommand{\etal}{{\em et al.}}

\lstset{
  captionpos=t,
  tabsize=2,
  numbers=left,
  numberstyle=\tiny,
  numbersep=5pt,
  breaklines=true,
  showstringspaces=true,
  basicstyle=\footnotesize\ttfamily,
  frame=none,
  emph={label},
  escapeinside={(*}{*)}
}

\begin{document}

\title{Computer Go Program}
\subtitle{Report for the implementation \texttt{gogogo}}

\authorinfo{Xiao Jia}
           {5090379042}
           {stfairy@gmail.com}

\maketitle

\begin{abstract}
The general weakness of computer Go programs compared with computer chess programs 
  has served to generate research into many new programming techniques.
Considering the difficulty of computer Go, we devised a combination of three 
  approaches: heuristics based on stone power, pattern recognition, and Monte Carlo. 
In this report, our approach is presented along with the system architecture, 
  the implementation details, and the evaluation result. 
Preliminary experiment results show that our approach is effective with respect to 
  the simplicity and generality of the implementation.
\end{abstract}

\section{Introduction}\label{sec:intro}

Computer Go is the field of artificial intelligence (AI) dedicated to creating 
  a computer program that plays Go, a traditional board game.
Go has long been considered a difficult challenge in the field of AI and 
  is considerably more difficult to solve than chess.
The techniques which proved to be the most effective in computer chess have 
  generally shown to be mediocre at Go.

Considering the difficulty of computer Go, we devised a combination of three 
  approaches:
\begin{enumerate}
  \item calculating heuristic values as priorities for each position
  \item pattern recognition
  \item Monte Carlo algorithm based on random playing
\end{enumerate}
We implement these approaches under a multi-phase framework \cite{Cai07} 
which is described in Section \ref{sec:algoimpl}.

Our team consists of two members:
\begin{description}
  \item[Shijian Li (5090379043)] is responsible for (i) \texttt{e3gtp}, which is a
    stable and reusable implementation of the GTP protocol in Java but not used 
    in our project in consideration of simplicity and efficiency; 
    (ii) the pattern filter (see Section \ref{sec:pattern-filter}), which is a 
    filter for pattern recognition and improves the overall performance dramatically.
    He works on parameter tuning and testing as well.
  \item[Xiao Jia (5090379042)] is responsible for (i) building the application and 
    algorithm framework, which plays an important role in our implementation and 
    eases the burden of trying out new ideas; (ii) setting up the skeleton of the 
    phases and basic filters for expanding territories and capturing stones 
    aggressively; (iii) implementing Monte Carlo algorithm based on random playing 
    agents in order to choose a good position in case there is a tie on the scores 
    of the heuristics which are defined by the filters. 
\end{description}

\subsection{Related Work}

M\"{u}ller \etal \cite{Muller95} claims that combinatorial game theory can be applied to computer Go,
and develops a sum game model for heuristic Go programming and a program for perfect play in late stage endgames. 
We devise power-related approaches with respect to the influence functions introduced in \cite{Muller95}.
We also adopt some of the ideas from the patterns discussed in \cite{Muller95}.

Kishimoto \etal \cite{Kishimoto04} presents a practical solution to the GHI (Graph History Interaction) 
problem that combines and extends previous techniques. Making use of this algorithm will reduce 
the time cost for searching in Go program. However, considering the complexity of implementing this 
algorithm under our framework, it is not used in our implementation because the actual time cost in 
\texttt{gogogo} is reasonable and durable. 

Gelly \etal \cite{Gelly06} develops a Monte-Carlo Go program using UCT (Upper bound Confidence for Tree), 
and provides several patterns in the Go game. We adopt the idea of patterns and use some of the 
patterns in \cite{Gelly06} as well as other patterns devised by ourselves. The use of patterns 
improves the performance dramatically. 

Cai \etal \cite{Cai07} surveys methods and some related works used in computer Go, 
and offers a basic overview for future study. They also present their attempts and 
simulation results in building a non-knowledge game engine, using a novel hybrid 
computation algorithm. We adopt the idea of phases and use it in our multi-phase 
framework. 

\section{System Architecture}\label{sec:sysarch}

\begin{figure}
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=0.3\textwidth]{eps/layers}
    \caption{\texttt{gogogo} is built on top of \texttt{brown}}
    \label{fig:layers}
  \end{subfigure}
  
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{eps/classes}
    \caption{Class diagram of \texttt{gogogo}}
    \label{fig:classes}
  \end{subfigure}
  
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{eps/genmove}
    \caption{Sequence diagram of move generation}
    \label{fig:genmove}
  \end{subfigure}
  
  \caption{System overview}\label{fig:sysoverview}
\end{figure}

Figure \ref{fig:sysoverview} provides an overview of the \texttt{gogogo} system. 
In general, \texttt{gogogo} is built on top of \texttt{brown} for simplicity, 
as shown in Figure \ref{fig:layers} so that the GTP protocol interface 
implementation can be reused. Figure \ref{fig:classes} shows all the classes 
in \texttt{gogogo}. On generating a move, the function \texttt{generate\_move} 
in \texttt{brown} will be invoked, which is modified to invoke the 
\texttt{selectMove} method of the \texttt{Policy} singleton. Figure \ref{fig:genmove}
shows the consequent method invocations for generating a move. 

\begin{figure}
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{eps/entities}
    \caption{
      Entities relationship:
      A policy has one or more phases while a phase has one or more filters.
      In reality, the policy is a singleton in our implementation.}
    \label{fig:entities}
  \end{subfigure}
  
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{eps/phases}
    \caption{
      Phases in a single policy:
      Arrows indicate possible transitions.
      The policy keeps track of the current phase, and 
      falls through to the next phase only if none of the 
      filters in the phase can generate a move.}
    \label{fig:phases}
  \end{subfigure}
  
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=0.9\textwidth]{eps/filters}
    \caption{
      Filters in a single phase:
      Filters are matched one by one. 
      The matching fails only if none of the filters matches.
      Once a filter matches and generates a move, 
      the matching stops and the policy uses that move.
      In this case, the current phase is not changed.}
    \label{fig:filters}
  \end{subfigure}
  
  \caption{Relationships among the policy, phases and filters}\label{fig:policy-phases-filters}
\end{figure}

As described in Section \ref{sec:intro}, we make use of a multi-phase approach. 
Figure \ref{fig:policy-phases-filters} depicts the relationships among the policy, 
phases and filters. The policy is a singleton and is the entrance of the whole algorithm. 
By design, a policy has one or more phases. In our implementation, there are 4 phases: 
\begin{description}
  \item[Opening phase] is dedicated to occupying 9 special positions on the board.
  \item[Running phase] is the phase which tries to chase the enemy, making small eyes, and capturing stones. 
  \item[Closing phase] is responsible for the situation where the board is almost full. It will try to fill empty positions and to make small eyes.
  \item[Passing phase] is a \emph{sentinel} phase which always generates a pass move.
\end{description}
A phase consists of many filters. Each filter is responsible for matching a specific 
pattern or trying to occupy important positions according to power-based heuristic values.
There are 6 kinds of filters:
\begin{description}
  \item[\texttt{FixposFilter}] is the filter which matches certain positions on the board (used for the opening phase). 
  \item[\texttt{PowerFilter}] is the filter which calculates \emph{descending power values}. \emph{Power} is a very important concept in our approach which is described in Section \ref{sec:power-filter}.
  \item[\texttt{PositiveFilter}] works in the same way as \texttt{PowerFilter} except that it doesn't care enemy stones.
  \item[\texttt{RandomFilter}] consists of two child filters. It chooses the first filter with probability $p$ and the second filter with probability $1-p$.
  \item[\texttt{ProductFilter}] consists of two child filters and combines the effects of the two filters. 
  \item[\texttt{PatternFilter}] does pattern recognition and counts the number of patterns matched on each position as its power value. It is described in Section \ref{sec:pattern-filter}. 
\end{description}

\section{Algorithms and Implementation}\label{sec:algoimpl}

\begin{lstlisting}[label=code:policy,caption=\texttt{Policy::selectMove(moves, \&pt)}]
pair<int, int> pt;
while (!currentPhase->selectMove(moves, pt)) {
  currentPhase = currentPhase->getNextPhase();
}
return pt;
\end{lstlisting}

Listing \ref{code:policy} shows the skeleton of \texttt{selectMove} method of the policy,
which tries to generate a move using the current phase, or falls through to the next phase if 
no move can be selected. 

\begin{lstlisting}[label=code:phase,caption=\texttt{Phase::selectMove(moves, \&pt)}]
for (int i = 0; i < filters.size(); i++) {
  Filter *f = filters[i];
  vector<pair<int, int>> fm = f->filter(moves);
  if (fm.empty()) continue;
  pt = randomChoose(bestMoves(fm));
  return true;
}
return false;
\end{lstlisting}

Listing \ref{code:phase} shows the process of \texttt{selectMove} 
method of a phase, which iterates over the filters one by one, trying to filter out a move. 
If the filter returns an empty list of moves, it goes to the next filter. Otherwise, 
it randomly chooses one of the best moves returned from the filter. 
The most important thing here is how to find the \emph{best moves}. In section \ref{sec:monte-carlo},
we show how to evaluate moves according to the winning rate. 

\begin{lstlisting}[label=code:filter,caption=\texttt{Filter::filter(moves)}]
vector<vector<double>> pm = generatePowerMap();
double mp = maxPower(pm, moves);
vector<pair<int, int>> filtered;
pair<double, double> t = getThresholds();
if (t.first <= mp && mp <= t.second)
  for (int k = 0; k < moves.size(); k++) {
    pair<int, int> p = moves[k];
    int i = p.first, j = p.second;
    if (pm[i][j] == mp) filtered.push_back(p);
  }
return filtered;
\end{lstlisting}

Listing \ref{code:filter} shows the general filtering process of a filter. 
First of all, the filter generates a power map which maps each position on 
the board to a real number, as the \emph{power value}. 
Then the maximum power value of the moves is 
compared with the thresholds of the filter. If the maximum power value 
is in the range of the thresholds, the filter will return all the positions 
with the maximum power value. Otherwise, an empty list is returned. 

\subsection{Power Filter}\label{sec:power-filter}

\texttt{PowerFilter} is one of the most important filters in our system. 
It will spread out powers from one's own side stones and consider enemy stones 
which will weaken one's own side powers. The saying of \texttt{generatePowerMap} 
originates just from \texttt{PowerFilter}. Take a look at the following example 
in order to get a general idea of how \texttt{PowerFilter} works. 

\begin{example}\label{eg:power}
  \begin{figure}
    \centering
    \begin{subfigure}[b]{0.18\textwidth}
      \centering
      \hspace{8mm}\begin{psgopartialboard}[9]{(4,4)(6,6)}
        \stone{white}{d}{5}
        \stone{white}{e}{6}
        \stone{white}{f}{5}
        \stone{black}{e}{5}
      \end{psgopartialboard}
      \vspace{2mm}\caption{Original board}\label{bd:power}
    \end{subfigure}
    \begin{subfigure}[b]{0.28\textwidth}
      \centering
      \hspace{8mm}\begin{psgopartialboard}[9]{(4,4)(6,6)}
        \stone{white}{d}{5}
        \stone{white}{e}{6}
        \stone{white}{f}{5}
        \stone{black}{e}{5}
        \markpos{\marksq}{e}{4}
      \end{psgopartialboard}
      \vspace{2mm}\caption{Selected move (marked by square)}\label{bd:power-cross}
    \end{subfigure}
    \caption{Partial boards for Example \ref{eg:power}}
  \end{figure}
  Consider the board in Figure \ref{bd:power}. 
  Suppose the \texttt{PowerFilter} is based on the following matrix
  \begin{equation*}
  \begin{bmatrix}
    2 & 1 & 2 \\
    1 & 0 & 1 \\
    2 & 1 & 2 \\
  \end{bmatrix}
  \end{equation*}
  Then the power map for the white color is 
  \begin{equation*}
  \begin{bmatrix}
    -2 & -1 & -2 \\
    -1 & 0  & -1 \\
    -2 & -1 & -2 \\
  \end{bmatrix}
  +
  \begin{bmatrix}
    1 & 0 & 1 \\
    2 & 1 & 2 \\
    0 & 0 & 0 \\
  \end{bmatrix}
  +
  \begin{bmatrix}
    1 & 2 & 0 \\
    0 & 1 & 0 \\
    1 & 2 & 0 \\
  \end{bmatrix}
  +
  \begin{bmatrix}
    0 & 2 & 1 \\
    0 & 1 & 0 \\
    0 & 2 & 1 \\
  \end{bmatrix}
  \end{equation*}
  which is eventually 
  \begin{equation*}
  \begin{bmatrix}
    0  & 3 & 0  \\
    1  & 3 & 1  \\
    -1 & 3 & -1 \\
  \end{bmatrix}
  \end{equation*}
  By choosing the maximum power, which is 3, 
  the filter will generate a move at the position 
  marked by the open square in Figure \ref{bd:power-cross}.
\end{example}

\subsubsection{Connecting Stones}\label{sec:connect-stone}

This section contains the filters for connecting one's own side stones. 
Thresholds are omitted for brevity since they do not matter much in this case.

\begin{equation}
\begin{bmatrix}
  1 & 1 & 1 \\
  1 & 0 & 1 \\
  1 & 1 & 1 \\
\end{bmatrix}
\end{equation}

\begin{equation}
\begin{bmatrix}
  0 & 0 & 0 &  1 & 0 & 0 & 0 \\
  0 & 0 & 1 &  2 & 1 & 0 & 0 \\
  0 & 1 & 3 &  5 & 3 & 1 & 0 \\
  1 & 2 & 5 & 10 & 5 & 2 & 1 \\
  0 & 1 & 3 &  5 & 3 & 1 & 0 \\
  0 & 0 & 1 &  2 & 1 & 0 & 0 \\
  0 & 0 & 0 &  1 & 0 & 0 & 0 \\
\end{bmatrix}
\end{equation}

\subsubsection{Making Eyes}\label{sec:make-eye}

This section contains the filters for making eyes. 

\begin{equation}
\begin{bmatrix}
  0 & 0  & 1  & 0  & 0 \\
  0 & 1  & -1 & 1  & 0 \\
  1 & -1 & 0  & -1 & 1 \\
  0 & 1  & -1 & 1  & 0 \\
  0 & 0  & 1  & 0  & 0 \\
\end{bmatrix}
\end{equation}

\begin{equation}
\begin{bmatrix}
    0 & 1.5 &  1 & 1.5 &   0 \\
  1.5 & 1.8 & -1 & 1.8 & 1.5 \\
    1 &  -1 &  0 &  -1 &   1 \\
  1.5 & 1.8 & -1 & 1.8 & 1.5 \\
    0 & 1.5 &  1 & 1.5 &   0 \\
\end{bmatrix}
\end{equation}

\subsection{Pattern Filter}\label{sec:pattern-filter}

Pattern recognition is used in pattern filters. We adopt the patterns 
introduced in \cite{Gelly06} and consider each of them with 8 permutations. 
With local patterns, we can chase enemy, prevent the enemy from making eyes, 
compass enemy, capture enemy stones, etc. All the patterns are listed in 
Figure \ref{fig:patterns}. 

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \stone{black}{d}{6}
      \stone{white}{e}{6}
      \stone{black}{f}{6}
      \markpos{\marksq}{e}{5}
      \markpos{\markma}{d}{4}
      \markpos{\markma}{e}{4}
      \markpos{\markma}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \stone{black}{d}{6}
      \stone{white}{e}{6}
      \markpos{\marksq}{e}{5}
      \markpos{\markma}{d}{4}
      \markpos{\markma}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \stone{black}{d}{6}
      \stone{white}{e}{6}
      \markpos{\markma}{f}{6}
      \stone{black}{d}{5}
      \markpos{\marksq}{e}{5}
      \markpos{\markma}{d}{4}
      \markpos{\markma}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \stone{black}{d}{6}
      \stone{white}{e}{6}
      \stone{white}{f}{6}
      \stone{black}{e}{5}
      \markpos{\marksq}{e}{5}
      \markpos{\markma}{d}{4}
      \markpos{\markma}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  %%%
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \stone{black}{d}{6}
      \stone{white}{e}{6}
      \markpos{\markma}{f}{6}
      \stone{white}{d}{5}
      \markpos{\marksq}{e}{5}
      \markpos{\markma}{f}{5}
      \markpos{\markma}{d}{4}
      \markpos{\markma}{e}{4}
      \markpos{\markma}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \stone{black}{d}{6}
      \stone{white}{e}{6}
      \markpos{\markma}{f}{6}
      \stone{white}{d}{5}
      \markpos{\marksq}{e}{5}
      \stone{white}{f}{5}
      \markpos{\markma}{d}{4}
      \markpos{\markma}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \stone{black}{d}{6}
      \stone{white}{e}{6}
      \markpos{\markma}{f}{6}
      \stone{white}{d}{5}
      \markpos{\marksq}{e}{5}
      \markpos{\markma}{d}{4}
      \stone{white}{e}{4}
      \markpos{\markma}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  %%%
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard}[9]{(4,4)(6,6)}
      \markpos{\markma}{d}{6}
      \stone{black}{e}{6}
      \markpos{\markma}{f}{6}
      \stone{white}{d}{5}
      \markpos{\marksq}{e}{5}
      \stone{white}{f}{5}
      \stone{white}{d}{4}\markpos{\markdd}{d}{4}
      \stone{white}{e}{4}\markpos{\markdd}{e}{4}
      \stone{white}{f}{4}\markpos{\markdd}{f}{4}
    \end{psgopartialboard}
    \vspace{2mm}
  \end{subfigure}
  %%%
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard*}[9]{(2,1)(4,2)}
      \stone{black}{b}{2}
      \markpos{\markma}{d}{2}
      \stone{white}{b}{1}
      \markpos{\marksq}{c}{1}
      \markpos{\markma}{d}{1}
    \end{psgopartialboard*}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard*}[9]{(2,1)(4,2)}
      \markpos{\markma}{b}{2}
      \stone{black}{c}{2}
      \markpos{\markma}{d}{2}
      \markpos{\markdd}{b}{1}\stone{black}{b}{1}
      \markpos{\marksq}{c}{1}
      \stone{white}{d}{1}
    \end{psgopartialboard*}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard*}[9]{(2,1)(4,2)}
      \markpos{\markma}{b}{2}
      \stone{black}{c}{2}
      \stone{white}{d}{2}
      \markpos{\markma}{b}{1}
      \stone{black}{c}{1}\markpos{\marksq}{c}{1}
      \markpos{\markma}{d}{1}
    \end{psgopartialboard*}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard*}[9]{(2,1)(4,2)}
      \markpos{\markma}{b}{2}
      \stone{black}{c}{2}
      \stone{white}{d}{2}
      \markpos{\markma}{b}{1}
      \stone{white}{c}{1}\markpos{\marksq}{c}{1}
      \markpos{\markdd}{d}{1}\stone{black}{d}{1}
    \end{psgopartialboard*}
  \end{subfigure}
  \begin{subfigure}[b]{0.15\textwidth}
    \centering
    \begin{psgopartialboard*}[9]{(2,1)(4,2)}
      \markpos{\markma}{b}{2}
      \stone{black}{c}{2}
      \stone{white}{d}{2}
      \stone{white}{b}{1}
      \stone{white}{c}{1}\markpos{\marksq}{c}{1}
      \stone{black}{d}{1}
    \end{psgopartialboard*}
  \end{subfigure}
  \caption{Patterns in \texttt{gogogo}}\label{fig:patterns}
\end{figure}

\subsection{Monte Carlo}\label{sec:monte-carlo}

Monte Carlo is used when a filter returns multiple candidate moves. 
In that case, we use Monte Carlo to simulate 100 or more times for each move. 
Each simulation is just random playing until both sides generate pass moves. 
Finally, the candidate move with the largest winning rate is selected.

\section{Evaluation}\label{sec:eval}

Three evaluation tests (see Table \ref{tab:tests})
are run among three Go programs: \texttt{gogogoX}, 
\texttt{gogogo} and \texttt{brown}. \texttt{gogogoX} is the version without 
pattern recognition, \texttt{gogogo} is the submitted version, and 
\texttt{brown} is the random playing version. Each test is run for 50 times, 
and statistics is given in Table \ref{tab:eval}.

\begin{table}
  \centering
  \begin{tabular}{c|c|c}
    \hline
    Test & Black & White \\
    \hline
    1 & \texttt{gogogoX} & \texttt{brown} \\
    2 & \texttt{gogogo} & \texttt{brown} \\
    3 & \texttt{gogogo} & \texttt{gogogoX} \\
    \hline
  \end{tabular}
  \caption{Evaluation tests}\label{tab:tests}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{c|c|c|c|c|c}
    \hline
    \multirow{2}{*}{Test} & \multirow{2}{*}{Black wins} & \multicolumn{4}{|c}{Black scores} \\
    \cline{3-6}
    & & avg & min & max & dev \\
    \hline
    1 & $94\%$ & $22.9 (\pm 2.9)$ & $-23.5$ & $82.5$ & $20.5$ \\
    2 & $100\%$ & $81.8 (\pm 4.2)$ & $18.5$ & $147.5$ & $30.0$ \\
    3 & $88\%$ & $17.8 (\pm 2.7)$ & $-15.5$ & $79.5$ & $18.9$ \\
    \hline
  \end{tabular}
  \caption{Evaluation result}\label{tab:eval}
\end{table}

% \appendix
% \section{Appendix Title}
% 
% This is the text of the appendix, if you need one.

\bibliographystyle{abbrvnat}

% The bibliography should be embedded for final submission.

\bibliography{go}
% \begin{thebibliography}{}
% \softraggedright
% \end{thebibliography}

\end{document}
