#include "filter.h"
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include "logger.h"
#include <cstdlib>
using namespace std;

vector<pair<int, int> > Filter::filter(int board_size, int board[], int color, const vector<pair<int, int> > &moves) {
  vector<vector<double> > power_map = generatePowerMap(board_size, board, color);
  // cerr << "Filter::filter after generatePowerMap" << endl;
  double max_power = -1e9;
  for (size_t k = 0; k < moves.size(); k++) {
    pair<int, int> p = moves[k];
    int i = p.first;
    int j = p.second;
    max_power = max(max_power, power_map[i][j]);
  }
  Logger::getInstance().debug("Filter::filter max_power=" + to_s(max_power));
  vector<pair<int, int> > filtered;
  pair<double, double> th = getThresholds();
  if (th.first <= max_power && max_power <= th.second) {
    for (size_t k = 0; k < moves.size(); k++) {
      pair<int, int> p = moves[k];
      int i = p.first;
      int j = p.second;
      if (power_map[i][j] == max_power)
        filtered.push_back(p);
    }
    Logger::getInstance().info(getFilterName() + ": filtered.size=" + to_s(filtered.size()));
  } else {
    Logger::getInstance().info(getFilterName() + ": out of thresholds");
  }
  return filtered;
}
