#include "policy.h"
#include <iostream>
#include <vector>
#include <utility>
#include "phase.h"
#include "opening_phase.h"
#include "running_phase.h"
#include "closing_phase.h"
#include "passing_phase.h"
#include "logger.h"
using namespace std;

Policy* Policy::_instance = NULL;

Policy& Policy::getInstance() {
  if (_instance == NULL) {
    _instance = new Policy();
  }
  return *_instance;
}

void Policy::destroyInstance() {
  if (_instance) {
    delete _instance;
    _instance = NULL;
  }
}

Policy::Policy() {
  opening_phase = new OpeningPhase();
  running_phase = new RunningPhase();
  closing_phase = new ClosingPhase();
  passing_phase = new PassingPhase();
  
  current_phase = opening_phase;
  opening_phase->setNextPhase(running_phase);
  running_phase->setNextPhase(closing_phase);
  closing_phase->setNextPhase(passing_phase);
  passing_phase->setNextPhase(passing_phase); // passing phase always generate a pass move
  
  search_flag = true;
  
  Logger::unlinkLogFile();
}

pair<int, int> Policy::selectMove(int board_size, int board[], int color, const vector<pair<int, int> > &moves) {
  Logger::getInstance().info("Policy::selectMove current_phase is " + current_phase->getPhaseName());
  pair<int, int> pt;
  while (!current_phase->selectMove(board_size, board, color, moves, pt)) {
    Logger::getInstance().info("Policy::selectMove while loop");
    current_phase = current_phase->getNextPhase();
  }
  Logger::getInstance().info("Policy::selectMove returning " + to_s(pt.first) + "," + to_s(pt.second));
  return pt;
}
