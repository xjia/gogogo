#include "closing_phase.h"
#include "power_filter.h"

using std::string;

ClosingPhase::ClosingPhase() {
  addFilter(new PowerFilter(
    5,
    "0  0  1  0  0 "
    "0  1 -1  1  0 "
    "1 -1  0 -1  1 "
    "0  1 -1  1  0 "
    "0  0  1  0  0 ",
    -10, 10
  ));
}

string ClosingPhase::getPhaseName() {
  return "ClosingPhase";
}
