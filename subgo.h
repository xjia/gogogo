#ifndef SUBGO_H_
#define SUBGO_H_

#include "gogogo.h"

class subgo
{
public:
  subgo(int board_size);
  virtual ~subgo() {}
  
  void init_gogogo(void);
  void clear_board(void);
  int board_empty(void);
  int get_board(int i, int j);
  int get_string(int i, int j, int *stonei, int *stonej);
  int legal_move(int i, int j, int color);
  void play_move(int i, int j, int color);
  void generate_move(int *i, int *j, int color);
  void compute_final_status(void);
  int get_final_status(int i, int j);
  void set_final_status(int i, int j, int status);
  int valid_fixed_handicap(int handicap);
  void place_fixed_handicap(int handicap);
  void place_free_handicap(int handicap);
  int suicide(int i, int j, int color);

protected:
  int pass_move(int i, int j);
  int on_board(int i, int j);
  int has_additional_liberty(int i, int j, int libi, int libj);
  int provides_liberty(int ai, int aj, int i, int j, int color);
  int remove_string(int i, int j);
  int same_string(int pos1, int pos2);
  void set_final_status_string(int pos, int status);

private:
  int board_size;
  float komi;
  int board[MAX_BOARD * MAX_BOARD];
  int next_stone[MAX_BOARD * MAX_BOARD];
  int final_status[MAX_BOARD * MAX_BOARD];
  int ko_i, ko_j;
};

#endif
