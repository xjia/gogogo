#ifndef POWER_FILTER_H_
#define POWER_FILTER_H_

#include <string>
#include <vector>
#include "filter.h"

class PowerFilter : public Filter {
public:
  PowerFilter(int n, const std::string &power_levels, double l, double h);
  virtual ~PowerFilter() {}
  virtual std::string getFilterName();
  virtual std::pair<double, double> getThresholds();
  virtual std::vector<std::vector<double> > generatePowerMap(int board_size, int board[], int color);
private:
  int n;
  std::vector<std::vector<double> > power_level;
  double low, high;
  void spread_out_power(int i, int j, int sign, std::vector<std::vector<double> > &board_power);
};

#endif
