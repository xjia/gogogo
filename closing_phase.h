#ifndef CLOSING_PHASE_H_
#define CLOSING_PHASE_H_

#include "phase.h"

class ClosingPhase : public Phase {
public:
  ClosingPhase();
  virtual ~ClosingPhase() {}
  virtual std::string getPhaseName();
};

#endif
