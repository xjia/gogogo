#include "running_phase.h"
#include "positive_filter.h"
#include "power_filter.h"

using std::string;

RunningPhase::RunningPhase() {
  addFilter(new PowerFilter(
    5,
    "  0  1.5   1  1.5    0 "
    "1.5  1.8  -1  1.8  1.5 "
    "  1   -1   0   -1    1 "
    "1.5  1.8  -1  1.8  1.5 "
    "  0  1.5   1  1.5    0 ",
    3.7, 5.3
  ));
  addFilter(new PositiveFilter(
    5,
    "0  0  1  0  0 "
    "0  1 -1  1  0 "
    "1 -1  0 -1  1 "
    "0  1 -1  1  0 "
    "0  0  1  0  0 ",
    2, 3
  ));
  addFilter(new PositiveFilter(
    3,
    " 1  1  1 "
    " 1  0  1 "
    " 1  1  1 ",
    2, 2
  ));
  addFilter(new PowerFilter(
    9,
    "0  0  0  0  0  0  0  0  0 "
    "0  0  0  0  1  0  0  0  0 "
    "0  0  0  1  2  1  0  0  0 "
    "0  0  1  3  5  3  1  0  0 "
    "0  1  2  5 10  5  2  1  0 "
    "0  0  1  3  5  3  1  0  0 "
    "0  0  0  1  2  1  0  0  0 "
    "0  0  0  0  1  0  0  0  0 "
    "0  0  0  0  0  0  0  0  0 ",
    -15, 10000
  ));
}

string RunningPhase::getPhaseName() {
  return "RunningPhase";
}
