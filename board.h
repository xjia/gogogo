#ifndef BOARD_H_
#define BOARD_H_

#include <vector>
#include "subgo.h"

class board_t
{
public:
  board_t(int in_board_size, int in_board[]);
  virtual ~board_t() {}
  
  void put(int color, int row, int col);
  bool random_play(int color);
  int score_of(int color) const;

protected:
  bool on_board(int i, int j) const { return i >= 0 && i < board_size && j >= 0 && j < board_size; }

private:
  int board_size;
  std::vector<std::vector<int> > board;
  std::vector<int> score;
  subgo sub;
};

#endif
