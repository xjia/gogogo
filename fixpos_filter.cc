#include "fixpos_filter.h"
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include "logger.h"
using namespace std;

string FixposFilter::getFilterName() {
  return "FixposFilter";
}

pair<double, double> FixposFilter::getThresholds() {
  return make_pair(1, 1);
}

vector<vector<double> > FixposFilter::generatePowerMap(int board_size, int board[], int color) {
  // cerr << "FixposFilter::generatePowerMap" << endl;
  const int d = (board_size - n) / 2;
  vector<vector<double> > power_map(board_size, vector<double>(board_size, 0));
  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++) {
      // cerr << "FixposFilter::generatePowerMap i=" << i << " j=" << j << endl;
      if (board_pattern[i * n + j] != '.' && on_board(i + d, j + d))
        power_map[i + d][j + d] = 1;
    }
  // Logger::getInstance().debug("FixposFilter power_map");
  // for (int i = 0; i < board_size; i++) {
  //   string s = "";
  //   for (int j = 0; j < board_size; j++)
  //     s += to_s(power_map[i][j]);
  //   Logger::getInstance().debug(s);
  // }
  return power_map;
}
