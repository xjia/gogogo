#ifndef LOGGER_H_
#define LOGGER_H_

#include <string>
#include <sstream>

template<typename T>
static std::string to_s(const T &t) {
  std::ostringstream oss;
  oss << t;
  return oss.str();
}

class Logger {
public:
  static Logger& getInstance();
  static void unlinkLogFile();
protected:
  static Logger* _instance;

public:
  void trace(const std::string &msg) { log(msg, "TRACE"); }
  void info(const std::string &msg) { log(msg, "INFO"); }
  void debug(const std::string &msg) { log(msg, "DEBUG"); }
  void warn(const std::string &msg) { log(msg, "WARN"); }
  void error(const std::string &msg) { log(msg, "ERROR"); }
  void log(const std::string &msg, const std::string &level);
protected:
  Logger() {}
  virtual ~Logger() {}
};

#endif
