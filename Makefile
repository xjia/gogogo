HEADERS= gogogo.h gtp.h policy.h phase.h filter.h opening_phase.h running_phase.h closing_phase.h passing_phase.h fixpos_filter.h power_filter.h logger.h positive_filter.h board.h subgo.h
SOURCES= gogogo.cc gtp.cc interface.cc policy.cc phase.cc filter.cc opening_phase.cc running_phase.cc closing_phase.cc passing_phase.cc fixpos_filter.cc power_filter.cc logger.cc positive_filter.cc board.cc subgo.cc

gogogo:	$(HEADERS) $(SOURCES)
	g++ -O2 $(SOURCES) -o gogogo -Wall
