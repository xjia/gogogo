#ifndef PASSING_PHASE_H_
#define PASSING_PHASE_H_

#include "phase.h"

class PassingPhase : public Phase {
public:
  PassingPhase() {}
  virtual ~PassingPhase() {}
  virtual std::string getPhaseName();
  virtual bool selectMove(int board_size, int board[], int color, const std::vector<std::pair<int, int> > &moves, std::pair<int, int> &pt);
};

#endif
