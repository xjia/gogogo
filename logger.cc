#include "logger.h"
#include <cstdio>
#include <string>
using namespace std;

Logger* Logger::_instance = NULL;

Logger& Logger::getInstance() {
  if (_instance == NULL) {
    _instance = new Logger();
  }
  return *_instance;
}

void Logger::unlinkLogFile() {
  unlink("gogogo.log");
}

void Logger::log(const string &msg, const string &level) {
  FILE *fp = fopen("gogogo.log", "a+");
  if (fp) {
    fprintf(fp, "[%s] %s\n", level.c_str(), msg.c_str());
    fclose(fp);
  }
}
