#ifndef RUNNING_PHASE_H_
#define RUNNING_PHASE_H_

#include "phase.h"

class RunningPhase : public Phase {
public:
  RunningPhase();
  virtual ~RunningPhase() {}
  virtual std::string getPhaseName();
};

#endif
