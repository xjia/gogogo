#ifndef PHASE_H_
#define PHASE_H_

#include <string>
#include <vector>
#include <utility>
#include "filter.h"

class Phase {
public:
  Phase() {}
  virtual ~Phase() {}
  Phase* getNextPhase() const { return next_phase; }
  void setNextPhase(Phase* p) { next_phase = p; }
  virtual std::string getPhaseName() = 0;
  virtual bool selectMove(int board_size, int board[], int color, const std::vector<std::pair<int, int> > &moves, std::pair<int, int> &pt);
protected:
  void addFilter(Filter* f) { filters.push_back(f); }
private:
  Phase* next_phase;
  std::vector<Filter*> filters;
};

#endif
