#ifndef FILTER_H_
#define FILTER_H_

#include <vector>
#include <utility>
#include <string>
#include "gogogo.h"

class Filter {
public:
  Filter() {}
  virtual ~Filter() {}
  std::vector<std::pair<int, int> > filter(int board_size, int board[], int color, const std::vector<std::pair<int, int> > &moves);
protected:
  virtual std::string getFilterName() = 0;
  virtual std::pair<double, double> getThresholds() = 0;
  virtual std::vector<std::vector<double> > generatePowerMap(int board_size, int board[], int color) = 0;
  bool on_board(int i, int j) { return i >= 0 && i < board_size && j >= 0 && j < board_size; }
};

#endif
